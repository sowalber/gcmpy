# README for gcmpy
Python tools for setting up and post-processing data from the MITgcm model.

Please see the documentation online on [readthedocs](https://gcmpy.readthedocs.io/en/latest/), or look in the `docs/` folder. This is also the home repository of the online documentation.

This package has a few separate main packages within it:

 * netCDFbinary: Python package for converting MITgcm binary output files to easy to work with netCDF files
 * postprocessing: Python routines for visualization of MITgcm netCDF data files
 * utils: Various scripts and modules to help working with the model

# Installation
Clone or download and extract the code. Put the code directory on your python path, or add the directory to your python path by adding the following line to your `.bashrc` file

```bash
export PYTHONPATH="${PYTHONPATH}:/path/to/package"
```

# Requirements

## Python version
Becuase of matplotlib, python version >= 3.4 is required. The core modules have been written for python 3 only. Python 2.x is not supported.

## Python Packages
gcmpy requires the following python packages

* numpy
* [netCDF4](http://unidata.github.io/netcdf4-python/), which requires the netCDF and HDF5 C libraries.
* [MITgcmutils](https://github.com/MITgcm/MITgcm), using `$MITgcm/utils/python/MITgcmutils/steup.py`. See installation docs for more detail about installing this package.
* [f90nml](https://github.com/marshallward/f90nml) to parse Fortran namelists
* [matplotlib](https://matplotlib.org)

**Optional Packages**

* [cmocean](https://github.com/matplotlib/cmocean) for colourmaps (available on package index)
* 

# MPI Files
The Graham MPI file is in the main directory of this repository.
