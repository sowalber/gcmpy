# MITgcm post-processing python routines
These post-processing routines are modified from those written by J Barenboim in 2016. The interface remains largely the same, but the routines now expect [netCDF files](https://github.com/timghill/netCDFbinary) as inputs.

This package requires python >=3.4 according to matplotlib documentation.

Animations are made using the [Matplotlib FuncAnimation](https://matplotlib.org/api/_as_gen/matplotlib.animation.FuncAnimation.html#matplotlib.animation.FuncAnimation) class, and still images of each frame are also saved.
