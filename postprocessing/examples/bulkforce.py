GLOBAL_GIF_ARGS = { 'start_time' :      0,
                    'sec_per_iter' :    10.0,
                    'gif_folder_name' : 'GIF',
                    'image_folder_name':'PNG',
                    'fps' :             2,
                    'plot_type' :       'gs',
                    'iters' :           [1080*i for i in range(40)]
}

vmin = -10
vmax = 6


ARGS3D = [
                 { 'var' :              'T',
                   'movie_name' :       'exf_thsice_dt20_surface_T.gif',
                   'image_name' :       'exf_thsice_dt20_surface_T_.png',
                   'cut_var' :          'z',
                   'vmin' :             vmin,
                   'vmax' :             vmax,
                   'cut_val' :          0 },
                 { 'var' :              'T',
                   'movie_name' :       'exf_thsice_dt20_x_3000_T.gif',
                   'image_name' :       'exf_thsice_dt20_x_3000_T_.png',
                   'cut_var' :          'x',
                   'vmin' :             vmin,
                   'vmax' :             vmax,
                   'cut_val' :          3000 },
                 { 'var' :              'T',
                   'movie_name' :       'exf_thsice_dt20_y_1500_T.gif',
                   'image_name' :       'exf_thsice_dt20_y_1500_T_.png',
                   'cut_var' :          'y',
                   'vmin' :             vmin,
                   'vmax' :             vmax,
                   'cut_val' :          1500 }
        ]


ARGS2D = [
                 { 'var' :              'SI_Fract',
                   'movie_name' :       'exf_thsice_dt20_surface_ice.gif',
                   'image_name' :       'exf_thsice_dt20_surface_ice_.png',
                   'vmin' :             0,
                   'vmax' :             1,
                   'ice_velocity_field':False}
       ]
