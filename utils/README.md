# README for `gcmpy/utils`
This directory contains some helpful modules for making figures from MITgcm converted netCDF output, as well as some standalone scripts to make diagnostic plots.

See documentation in the docs folder, or on [readthedocs](https://gcmpy.readthedocs.io/en/latest/utils/README/)
