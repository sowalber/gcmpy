"""

deltaAverage2d compares the mean value of a given variable. Since you must specify
the variable, the command line interface has more options. The given variable
is averaged in all dimensions at each time step, and the mean value is plotted
as a function of time.

Command Line Interface:

python -m gcmpy.utils.deltaAverage2d file field --dirs --labels --title --ylabel [--start --stop --step --time_unit]

Where
 Required:
 * file is what to save the image as
 * field specifies the variable to average
 * dirs is a list of directories to compare data from
 * labels is a list of legend labels
 * title and ylabel specify what to label the plot with

 Optional:
 * start, step, stop specify which iterations to plot. The list of files and
    indices is indexed from start to stop, in stepsize step.
      files[start:stop:step]
 * time_unit is one of ('day', '1e3', or None or ''). 'day' plots x axis in
    days, '1e3' plots x axis in 1000 seconds, and none or '' plots in seconds
"""


import argparse
import os

import numpy as np
import netCDF4 as nc

import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt

from . import modeldata
from . import find
from . import interface
from . import timeutils

# Constant: specific heat capacity of water (J / (kg C) )
rho_ice = 916.75    # ice density (kg / m^3)

def makeplot(figname, field, ylabel, title, dirs, labels, start, stop, step, include_delta = False, timeunit = ''):
    """Compare the mean values of variable field between model runs"""
    filedata = [find.all(dir, step = step, start = start, stop = stop) for dir in dirs]
    mp = [modeldata.getdata(data = os.path.join(dir, 'data'), datapkg = os.path.join(dir, 'data.pkg')) for dir in dirs]

    nfiles = len(filedata[0].files)

    delta = [np.zeros(np.shape(fd.iters)) for fd in filedata]

    for j in range(len(delta)):
        for i in range(len(filedata[j].files)):
            print('Plotting file %s' % os.path.join(dirs[j], filedata[j].files[i]))

            try:
                data = nc.Dataset(os.path.join(dirs[j], filedata[j].files[i]), 'r')
                delta[j][i] = data[field][:].mean()
            except:
                print('Error reading file %s' % os.path.join(dirs[j], filedata[j].files[i]))
                delta[j][i] = np.nan

    times = [mp[j].deltaT * np.array(fd.iters) for j,fd in enumerate(filedata)]
    times, xlab = timeutils.convert(times, timeunit)

    if include_delta:
        fig, (ax1, ax2) = plt.subplots(figsize = (6, 6), sharex = True, nrows = 2)
        ax2.plot(times[0], delta[-1] - delta[0])
        ax2.grid()
        ax2.set_xlabel(xlab)
        ax2.set_ylabel('Delta %s' % ylabel)
        ax2.set_title('Delta %s' % title)

    else:
        fig, ax1 = plt.subplots()
        ax1.set_xlabel(xlab)

    for j, label in enumerate(labels):
        ax1.plot(times[j], delta[j], label = label)

    ax1.grid()
    ax1.legend()

    ax1.set_ylabel(ylabel)
    ax1.set_title(title)

    plt.tight_layout()

    fig.savefig(figname, dpi = 600)

def main():
    # Use base argument parser, add extra args.
    parser = interface.ArgumentParser()
    parser.add_argument('--title', help = 'Optional title for plots', default = '')
    parser.add_argument('--ylabel', help = 'Optional ylabel for plots', default = '')
    parser.add_argument('field', help = 'Which data field to average')
    args = parser.parse_args()

    makeplot(args.file, args.field, args.ylabel, args.title, args.dirs,
            args.labels, args.start, args.stop, args.step, args.include_delta,
            args.time_unit)

if __name__ == '__main__':
    main()
