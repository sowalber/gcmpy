"""

    deltaQ.py

    Module compares the total internal energy in the water between runs

    Uses standard command line interface defined in interface.py and in
    the documentation
"""


import argparse
import os

import numpy as np
import netCDF4 as nc

import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt

from . import modeldata
from . import find
from . import interface
from . import timeutils

# Constant: specific heat capacity of water (J / (kg C) )
c = 4186.0

# Constant: Density of water at 4C
rhoref = 1.e3

def main(figname, dirs, labels, start, stop, step, include_delta = False, time_unit = ''):
    """Compare total internal energy in water"""
    filedata = [find.all(dir, step = step, start = start, stop = stop) for dir in dirs]
    mp = [modeldata.getdata(data = os.path.join(dir, 'data'),
                                  datapkg = os.path.join(dir, 'data.pkg')) for dir in dirs]

    times = [mp[j].deltaT * np.array(fd.iters)/1.e3 for j,fd in enumerate(filedata)]

    deltaQs = [np.zeros(np.shape(fd.iters)) for fd in filedata]

    data0 = [nc.Dataset(os.path.join(dir, filedata[j].files[0])) for j,dir in enumerate(dirs)]
    T0 = [np.array(data['T']) for data in data0]
    for j in range(len(mp)):
        for i in range(len(filedata[j].files)-1):
            print('Plotting file %s' % os.path.join(dirs[j], filedata[j].files[i+1]))
            data = nc.Dataset(os.path.join(dirs[j], filedata[j].files[i+1]), 'r')
            T = np.array(data['T'])
            dT = T - T0[j]
            try:
                rho = rhoref + np.array(data['Rho'])
            except:
                rho = rhoref

            dQ = c * np.sum(rho * mp[j].dx * mp[j].dy * mp[j].dz * dT)
            deltaQs[j][i+1] = dQ


    times, xlab = timeutils.convert(times, time_unit)

    if include_delta:
        fig, (ax1, ax2) = plt.subplots(figsize = (6, 6), nrows = 2, sharex = True)

        ax2.plot(times[0], deltaQs[-1] - deltaQs[0])
        ax2.grid()
        ax2.set_xlabel(xlab)
        ax2.set_ylabel('Heat (J)')
        ax2.set_title('Delta heat transfer')

    else:
        fig, ax1 = plt.subplots()
        ax1.set_xlabel(xlab)

    for j,label in enumerate(labels):
        ax1.plot(times[j], deltaQs[j], label = label)

    ax1.grid()
    ax1.legend()
    ax1.set_ylabel('Heat (J)')
    ax1.set_title('Total internal energy of water')

    plt.tight_layout()

    fig.savefig(figname, dpi = 600)


if __name__ == '__main__':
    args = interface.multiple()
    main(args.file, args.dirs, args.labels, args.start, args.stop, args.step,
                                    include_delta = args.include_delta)
