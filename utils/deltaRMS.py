"""

    DECIDE WHETHER TO KEEP THIS

"""


import argparse
import os

import numpy as np
import netCDF4 as nc

import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt

import cmocean

from . import modeldata
from . import find
from . import interface

def main(figname, field, ylabel, title, dirs, labels, start, stop, step,
                                                        include_delta = False):
    filedata = [find.all(dir, step = step, start = start, stop = stop) for dir in dirs]
    mp = [modeldata.getdata(data = os.path.join(dir, 'data'), datapkg = os.path.join(dir, 'data.pkg')) for dir in dirs]

    nfiles = len(filedata[0].files)

    delta = [np.zeros(np.shape(fd.iters)) for fd in filedata]

    for j in range(len(delta)):
        for i in range(len(filedata[j].files)):
            print('Plotting file %s' % os.path.join(dirs[j], filedata[j].files[i]))

            try:
                data = nc.Dataset(os.path.join(dirs[j], filedata[j].files[i]), 'r')
                delta[j][i] = np.sqrt(np.mean(data[field][:]**2))
            except:
                print('Error reading file %s' % os.path.join(dirs[j], filedata[j].files[i]))
                delta[j][i] = np.nan

    times = [mp[j].deltaT * np.array(fd.iters)/1.e3 for j,fd in enumerate(filedata)]

    if include_delta:
        fig, (ax1, ax2) = plt.subplots(figsize = (6, 6), sharex = True, nrows = 2)
        ax2.plot(times[0], delta[-1] - delta[0])
        ax2.grid()
        ax2.set_xlabel('Time ($10^3$ s)')
        ax2.set_ylabel('Delta %s' % ylabel)
        ax2.set_title('Delta %s' % title)

    else:
        fig, ax1 = plt.subplots()
        ax1.set_xlabel('Time ($10^3$ s)')

    for j, label in enumerate(labels):
        ax1.plot(times[j], delta[j], label = label)

    ax1.grid()
    ax1.legend()

    ax1.set_ylabel(ylabel)
    ax1.set_title(title)

    plt.tight_layout()

    fig.savefig(figname, dpi = 600)



if __name__ == '__main__':
    parser = interface.ArgumentParser()
    parser.add_argument('field', help = 'Which data field to average')
    parser.add_argument('title', help = 'Title for the figure')
    parser.add_argument('--ylabel', help = 'Y-label for plot', default = '')
    args = parser.parse_args()

    main(args.file, args.field, args.ylabel, args.title, args.dirs,
                args.labels, args.start, args.stop, args.step,
                include_delta = args.include_delta)
