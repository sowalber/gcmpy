"""

    deltaTHETA.py

    Compare the total kinetic energy between hydrostatic and nonhydrostatic
    model configurations.

    Plots a line plot overlaying the TKE of the two configurations, and a
    line plot of the difference in TKE (convention is dynamic - static).

    Command-line interface:

    python -m gcmpy.utils.deltaTKE figname dir_static dir_dynamic [--step n]

     *  figname : File path to save the comparison plots as.
                  Saves direct comparison lineplot using exactly figname;
                  adds '_delta' before file extension for plot of the
                  difference in TKE.

     *  dir_static, dir_dynamic : The directories containing .nc files for
                                  hydrostatic and non-hydrostatic model
                                  configurations.

     *  step: Integer specifying step size when passing through all files.
                eg., if --step 4 is given, will only plot ever 4th file.

"""

import glob
import os
import argparse

import numpy as np
import netCDF4 as nc

import matplotlib
matplotlib.use('Agg')

from matplotlib import pyplot as plt

import cmocean

from . import modeldata
from . import find

rhoref = 1.E3

def main(figname, dirs, step = 1):
    filedata_d0 = find.all(dirs[0], step = step)
    filedata_d1 = find.all(dirs[1], step = step)

    datafile = os.path.join(dirs[1], 'data')
    datapkgfile = os.path.join(dirs[1], 'data.pkg')
    modelparams = modeldata.getdata(data = datafile, datapkg = datapkgfile)
    times = modelparams.deltaT * np.array(filedata_d0.iters) / 1.e3
    data = nc.Dataset(os.path.join(dirs[0], filedata_d0.files[0]))
    Z = np.array(data['zc']).min(axis = (1, 2))
    data.close()

    nfiles = len(filedata_d0.files)
    shape = (modelparams.nz, nfiles)
    tdata_d0 = np.zeros(shape)
    tdata_d1 = np.zeros(shape)

    for i in range(len(filedata_d0.files)):
        print("Plotting file %s" % filedata_d0.files[i])

        data_d0 = nc.Dataset(os.path.join(dirs[0], filedata_d0.files[i]))
        tdata_d0[:, i] = data_d0['T'][:].mean(axis = (1, 2))
        data_d0.close()

        data_d1 = nc.Dataset(os.path.join(dirs[1], filedata_d1.files[i]))
        tdata_d1[:, i] = data_d0['T'][:].mean(axis = (1, 2))
        data_d1.close()

    Tgrid, Zgrid = np.meshgrid(times, Z)

    fig, (ax1, ax2, ax3) = plt.subplots(nrows = 3, sharex = True, figsize = (6, 8))

    plt1 = ax1.pcolormesh(Tgrid, Zgrid, tdata_d0, cmap = cmocean.cm.thermal)
    plt2 = ax2.pcolormesh(Tgrid, Zgrid, tdata_d1, cmap = cmocean.cm.thermal)
    plt3 = ax3.pcolormesh(Tgrid, Zgrid, tdata_d1 - tdata_d0, cmap = cmocean.cm.delta)
    
    clim1 = plt1.get_clim()
    clim2 = plt2.get_clim()

    vmax = max(clim1[1], clim2[1])
    vmin = min(clim1[0], clim2[0])

    plt1.set_clim(vmin, vmax)
    plt2.set_clim(vmin, vmax)

    delta_clim = plt3.get_clim()
    cmax = np.max(np.abs(delta_clim))
    plt3.set_clim((-cmax, cmax))
    
    fig.colorbar(plt1, ax = (ax1, ax2))

    fig.colorbar(plt3, ax = ax3)

    ax3.set_xlabel('Time ($10^3$ s)')

    ax2.set_ylabel('Depth (m)')

    ax1.set_title('Hydrostatic temperature profile')
    ax2.set_title('Non-hydrostatic temeprature profile')
    ax3.set_title('Delta temperature profile')
    fig.savefig(figname, dpi = 600)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("file", help = "File name to save the figure as")
    parser.add_argument("dirs", help = "Directories to compare (hydrostatic, non-hydrostatic", nargs = 2)
    parser.add_argument("--step", help = "Skip value", type = int, default = 1)
    args = parser.parse_args()

    main(args.file, args.dirs, step = args.step)
