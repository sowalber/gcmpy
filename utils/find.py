"""

    find.py

    Finds all netCDF4 files and iterations in the given directory.

    Function all returns a list of iterations and a list of file names

"""

import os
import glob

import collections

def all(dir, start = 0, stop = None, step = 1):
    """Finds all netCDF4 files and returns sorted lists of files and iterations

    Returns:
        *   OrderedTuple with 'files' and 'iters' attributes
    """
    filedata = collections.namedtuple('Files', ('files', 'iters'))

    glob_pattern = os.path.join(dir, '*.nc')
    files = glob.glob(glob_pattern)
    files = [os.path.split(f)[-1] for f in files]
    strp = lambda s: int(s.split('_')[1].split('.')[0])
    files = sorted(files, key = strp)
    iters = [strp(f) for f in files]
    step = int(step)
    return filedata(files[start:stop:step], iters[start:stop:step])

def main():
    """Allow script execution.

    Defines a command-line interface:

    python -m find [dir]

    dir is optional, defaults to '.'
    """
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('dir', nargs= '?', default = '.')
    args = parser.parse_args()

    filedata = all(dir = args.dir)

    print("Files: %s" % ', '.join(filedata.files))
    print("Iters: %s" % ', '.join(filedata.iters))

if __name__ == '__main__':
    main()
