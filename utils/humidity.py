"""
    humidity.py

    Module to convert between relative humidity and specific humidity.

    Formulas and data taken from this data sheet
    https://www.cactus2000.de/js/calchum.pdf

    Constants:

    Symbol      Name                Units
    N           Avogadro Number     mol-1
    R           Gas constant        J mol-1 K-1
    Mh2o        Water molar mass    g mol-1
    Mdry        Air molar mass      g mol-1

    Variables:
    Name        Description         Units
    T           Temperature         C
    P           Pressure            Pa
    q           Specific humidity   unitless (kg/kg)
    RH          Relative humidity   % (ie, should be between 0 and 100)
    e           Vapor pressure      Pa

"""

# ===================================================================
#  Constants
N    = 6.0221415e23 # Avogadro, mol-1
R    = 8.3145       # Gas Constant, J mol-1 K-1
Mh2o = 18.01534     # Molar mass water, g mol-1
Mdry = 28.9644      # Molar mass dry air, g mol-1

# ===================================================================
#  Vapor pressure of water
# Coefficient array
A =     [   6.107799961,
            4.436518521e-1,
            1.428945805e-2,
            2.650648471e-4,
            3.031240396e-6,
            2.034080748e-8,
            6.136820929e-11,
        ]
a =     [   6.109177956,
            5.034698970e-1,
            1.886013408e-2,
            4.176223716e-4,
            5.824720280e-6,
            4.838803174e-8,
            1.838826904e-10,
        ]
def e(T):
    """Vapor pressure of water as a function of temperature"""
    Ewater =  A[0] + T*(A[1] + T*(A[2] + T*(A[3] + T*(A[4] + T*(A[5] + T*A[6])))))
    Eice   =  A[0] + T*(A[1] + T*(A[2] + T*(A[3] + T*(A[4] + T*(A[5] + T*A[6])))))
    e = min(Ewater, Eice) # this is in hPa
    return 100 * e  # Pa

# ===================================================================
#  Relative humidity
# Compute relative humidity from temperature, pressure and specific humidity
def RH(T, P, q):
    """Compute relative humidity from T, P, and specific humidity"""
    Xh2o = q*Mdry / ( q*Mdry + (1 - q) * Mh2o)
    Ph2o = P * Xh2o
    RH = 100 * Ph2o/e(T)
    return RH

# ===================================================================
#  Specific humidity
# Compute specific humidity from temperature, pressure and relative humidity
def q(T, P, RH):
    """Compute specific humidity from T, P, and relative humidity"""
    Ph2o = RH * e(T) / 100
    Xh2o = Ph2o / P
    q = Xh2o*Mh2o / ( Xh2o*Mh2o + (1 - Xh2o)*Mdry)
    return q

if __name__ == '__main__':
    # case 1: Convert relative to specific humidity.
    # T = -10 C, RH = 50 %, P = 101325 Pa (atmospheric pressure)
    rh = 50
    t = -10
    p = 101325.
    spec_humid = q(t, p, rh)
    print("Specific humdidity: %.8e" % spec_humid)

    # case 2: Convert specific humidity to relative humidity
    # T = 25 C, q = 0.01, P = 101325 Pa
    q = 0.01
    t = 25.
    p = 101325.
    rel_humid = RH(t, p, q)
    print("Relative humidity: %.8f%%" % rel_humid)
