"""

    tcolour.py

    Script to plot pcolormesh temperature profiles.

    Plots have Z on the y-axis, time on x-axis, and are coloured by
    temperature (Hovmoller diagrams).

"""

import argparse
import os

import numpy as np
import netCDF4 as nc

import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt

import cmocean

from . import modeldata
from . import find
from . import interface
from ..postprocessing import cmaps

def main(figname, start, stop, step, colmap, vmin, vmax):
    filedata = find.all('', start = start, stop = stop, step = step)
    modelparm = modeldata.getdata(data = 'data', datapkg = 'data.pkg')
    nfiles = len(filedata.files)
    shape = (modelparm.nz, nfiles)
    tdata = np.zeros(shape)

    for j,file in enumerate(filedata.files):
        print("Plotting file %s" % file)
        data = nc.Dataset(file)
        T = np.array(data['T'])
        tdata[:, j] = T.mean(axis = (1, 2))

    Zc = np.array(data['zc'])
    Z = Zc.min(axis = (1, 2))
    times = modelparm.deltaT * np.array(filedata.iters)
    Tgrid, Zgrid = np.meshgrid(times, Z)

    fig, ax = plt.subplots()
    tc = ax.pcolormesh(Tgrid, Zgrid, tdata, cmap = cmaps.getcm(colmap), vmin = vmin, vmax = vmax)
    cbar = fig.colorbar(tc)
    cbar.set_label("Temperature (C)")

    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Z (m)")

    ax.set_title("Horizontally averaged temperature profile")


    fig.savefig(figname)

if __name__ == '__main__':
    parser = interface.ArgumentParser(base = 'single')
    parser.add_argument('--cmap', help = 'Colourmap to use', default = 'cmocean/thermal')
    parser.add_argument('--vmin', help = 'Min colour scale', default = 0)
    parser.add_argument('--vmax', help = 'Max colour scale', default = 4)
    args = parser.parse_args()
    main(args.file, args.start, args.stop, args.step, args.cmap, vmin = args.vmin, vmax = args.vmax)
