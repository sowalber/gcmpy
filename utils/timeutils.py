"""

    timeutils.py

    Module to convert a time-series between time units.

    Supported units:
     * 'day' : convert time-series to be measured in days (divide by 86400.)
     * '1e3' : convert time-series to be measured in 1e3 seconds (divide by 1.e3)
     * '' or None: Keep time-series in seconds

    Usage:

        timeutils.convert(time_series, time_unit)

        Args:
         * time_series: iterable containing time values
         * time_unit: one of the above strings

        Returns:
         * times: numpy array with converted times
         * xlabel: nicely formatted string for time axis labels on plots.
          'day' --> 'Time (days)'
          '1e3' --> 'Time ($10^3$ s)'
          ''    --> 'Time (s)'

"""

import numpy as np

def convert(time_series, time_unit):
    """Convert time_series into units defined by time_unit.

    Return new time series and nice label with time units.
    """
    times = np.array(time_series)

    if time_unit == 'day':
        times = times / 86400.0
        xlab = 'Time (days)'

    elif time_unit == '1e3':
        times = times / 1.e3
        xlab = 'Time ($10^3$ s)'

    else:
        xlab = 'Time (s)'

    return (times, xlab)
