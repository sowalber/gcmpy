"""

    tprof.py

    Make animations of temperature profile over time.

    Plots have z on the y-axis, temperature on the x-axis, and time is animated.

    Command line interface is standard, with an additional argument 'stillfile'

    python -m gcmpy.utils.tprof anifile [stillfile] [--start --stop --stop --time_unit]

    Where if stillfile is given, it saves still frames as stillfile, inserting
    the iteration before the extension. Otherwise, does not save stills.

    In this case, time_unit controls how the time is formatted in the title.

"""
import os
import glob
import argparse

import numpy as np
import netCDF4 as nc

import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from matplotlib import animation

import MITgcmutils as mgu

from . import find
from . import modeldata
from . import interface

dpi = 300
fps = 4

def _title(time, time_unit):
    """Format frame title based on absolute time in seconds and time_unit.

    Returns a nicely formatted time label.
    """
    if time_unit == 'day':
        time = time / 86400.0
        labunit = 'days'
    elif time_unit == '1e3':
        time = time / 1.e3
        labunit = '$10^3$ s'
    else:
        title = 'Time'
        labunit = 's'

    title = 'T at t=%.6f %s' % (time, labunit)
    return title

def profile(figname, stillfile, start, stop, step, time_unit):
    filedata = find.all('', start, stop, step)
    mp = modeldata.getdata()

    times = mp.deltaT * np.array(filedata.iters)

    depth = mgu.rdmds('Depth').reshape(mp.ny, mp.nx, order= 'F')

    initdata = nc.Dataset(filedata.files[0], 'r')
    T = np.array(initdata['T'])
    Tavg = T.mean(axis = (1, 2))
    Z = np.array([np.sum(mp.delZ[:i]) for i in range(mp.nz)])
    
    Zc = np.array(initdata['zc'])
    T = np.ma.masked_where(np.abs(Zc) > depth, T)
    fig = plt.figure()
    ax = plt.subplot(111)

    lineplot, = ax.plot(Tavg, Z)
    ax.set_xlabel("Temperature (C)")
    ax.set_ylabel("z (m)")
    ax.set_title(_title(times[0], time_unit))

    ax.grid(True)
    ax.set_xlim(-1, 6)
    if stillfile:
        figbase = os.path.splitext(stillfile)[0]
        fig.savefig(figbase + '_%s.png' % filedata.iters[0], dpi = dpi)

    def animate(frame):
        print("Plotting file %s" % filedata.files[frame])
        ax.set_title(_title(times[frame], time_unit))

        iterdata = nc.Dataset(filedata.files[frame])
        Ti = np.ma.masked_where(np.abs(Zc) > depth, np.array(iterdata['T']))
        Tavg = Ti.mean(axis = (1, 2))
        lineplot.set_data(Tavg, Z)
        if stillfile:
            fig.savefig(figbase + '_%s.png' % iter, dpi = dpi)
        return lineplot

    frames = list(range(len(filedata.iters)))
    anim = animation.FuncAnimation(fig, animate, frames = frames,
                    blit = False, repeat = False)

    gifwriter = animation.ImageMagickFileWriter(fps = fps)
    anim.save(figname, writer = gifwriter)

if __name__ == "__main__":
    parser = interface.ArgumentParser()
    parser.add_argument('stillfile',
        help = 'Path to save still images as. If not given, does not save images',
        nargs = '?', default = None)
    args = parser.parse_args()

    profile(args.file, args.stillfile, args.start, args.stop, args.step, args.time_unit)
