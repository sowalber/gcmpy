"""

Plot mean surface temperature over time.

Command line interface is standard interface from documentation

"""

import glob
import os
import argparse

import numpy as np
import netCDF4 as nc

import matplotlib
matplotlib.use('Agg')

from matplotlib import pyplot as plt

from . import modeldata
from . import find
from . import interface
from . import timeutils

rho_ice = 916.75    # ice density (kg / m^3)

# fill value. Averages taken where data != fill
fill = -999.

def main(figname, start, stop, step, tunit = ''):
    filedata = find.all('', step = step, start = start, stop = stop)
    modelparams = modeldata.getdata()
    times = modelparams.deltaT * np.array(filedata.iters)

    avg = []

    for file in filedata.files:
        print("Plotting file %s" % file)
        data = nc.Dataset(file, 'r')
        arr = data['T'][0, :, :]
        avg.append(arr[arr!=fill].mean())

    times, xlab = timeutils.convert(times, tunit)

    fig, ax = plt.subplots()

    ax.set_xlabel(xlab)
    ax.set_ylabel('Temperature (C)')

    ax.plot(times, avg)

    ax.set_title('Mean surface temperature')
    ax.grid()

    fig.savefig(figname, dpi = 600)

if __name__ == "__main__":
    parser = interface.ArgumentParser()
    args = parser.parse_args()
    main(args.file, args.start, args.stop, args.step, args.time_unit)
